<?php

namespace App\Listeners;

use Aloha\Twilio\Twilio;
use App\CategoryWatch;
use App\Events\NewAuctionCreated;
use App\Mail\EmailNewAuctionCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class NewAuctionNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAuctionCreated  $event
     * @return void
     */
    public function handle(NewAuctionCreated $event)
    {
        // get the watches of category related to this new auction
        $category_watches = CategoryWatch::where('category_id', $event->auction->category_id)->get();

        // Emails all the other users else of this user for their outbidded auction
        foreach ($category_watches as $category_watch) {
            $user = $category_watch->owner();
            $auction_name = $event->auction->item_name;
            $category_name = $event->auction->category()->name;

            // Send an email if checked 'notify_email' option
            if ($category_watch->notify_email) {
                $email = new EmailNewAuctionCreated($user, $auction_name, $category_name);
                Mail::to($user->email)->send($email);
            }

            // Send a sms if checked 'notify_phone' option
            if ($category_watch->notify_phone && !is_null($user->phone)) {
                $message = "A new auction \"" . $auction_name . "\" is now open for bidding in the \"" . $category_name . "\" category - Entergy Bid Board. "
                            . "Visit the Entergy Bid Board for more information. "
                            . "Unsubscribe by visiting the \"" . $category_name . "\" category of the Bid Board.";
                $accountId = config('twilio.twilio.connections.twilio.sid');
                $token = config('twilio.twilio.connections.twilio.token');
                $fromNumber = config('twilio.twilio.connections.twilio.from');
                $twilio = new Twilio($accountId, $token, $fromNumber);
                try {
                    $twilio->message($user->phone, $message);
                } catch (\Services_Twilio_RestException $e) {
                    Log::error(
                        'Could not send SMS notification.' .
                        ' Twilio replied with Services_Twilio_RestException: The \'To\' number ' . $user->phone . ' is not a valid phone number.'
                    );
                }

                // Test the sms process via Nexmo on the local env
                /*
                Nexmo::message()->send([
                    'to' => 'user_number',
                    'from' => 'fake_number',
                    'text' => $message
                ]);
                */
            }
        }
    }
}
