<?php

namespace App\Listeners;

use Aloha\Twilio\Twilio;
use App\Events\OutBidSubmitted;
use App\Mail\EmailOutBidAuction;
use App\Watch;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Nexmo\Laravel\Facade\Nexmo;

class OutbidNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OutBidSubmitted  $event
     * @return void
     */
    public function handle(OutBidSubmitted $event)
    {
        // get the outbidded watches else when this user submitted a new bid
        $outbid_watches = Watch::where('auction_id', $event->bid->auction_id)
                                ->where('user_id', '!=', $event->bid->user_id)
                                ->get();

        // Emails all the other users else of this user for their outbidded auction
        foreach ($outbid_watches as $watch) {
            $outbid_user = $watch->owner();
            $auction_name = $event->bid->auction()->item_name;
            $bid_amount = $event->bid->amount;

            // Send an email if checked 'notify_email' option
            if ($watch->notify_email) {
                $email = new EmailOutBidAuction($outbid_user, $auction_name, $bid_amount);
                Mail::to($outbid_user->email)->send($email);
            }

            // Send a sms if checked 'notify_phone' option
            if ($watch->notify_phone && !is_null($outbid_user->phone)) {
                $message = "You've been outbid from the auction \"" . $auction_name . "\" - New high bid is $" . $bid_amount . " - Entergy Bid Board. "
                            . "Visit the Entergy Bid Board for more information. "
                            . "To unsubscribe, visit the auction page and disable the Alerts.";
                $accountId = config('twilio.twilio.connections.twilio.sid');
                $token = config('twilio.twilio.connections.twilio.token');
                $fromNumber = config('twilio.twilio.connections.twilio.from');
                $twilio = new Twilio($accountId, $token, $fromNumber);
                try {
                    $twilio->message($outbid_user->phone, $message);
                } catch (\Services_Twilio_RestException $e) {
                    Log::error(
                        'Could not send SMS notification.' .
                        ' Twilio replied with Services_Twilio_RestException: The \'To\' number ' . $outbid_user->phone . ' is not a valid phone number.'
                    );
                }

                // Test the sms process via Nexmo on the local env
                /*
                Nexmo::message()->send([
                    'to' => 'user_number',
                    'from' => 'fake_number',
                    'text' => $message
                ]);
                */
            }
        }
    }
}
