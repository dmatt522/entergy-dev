<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailNewAuctionCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $auction_name, $category_name)
    {
        $this->user = $user;
        $this->auction_name = $auction_name;
        $this->category_name = $category_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.auctions.new_created')
            ->subject('A new "' . $this->category_name . '" auction has been posted')
            ->with('auction_name' , $this->auction_name)
            ->with('category_name' , $this->category_name);
    }
}
