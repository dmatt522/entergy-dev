<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider, Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Add the custom validation rule for checking the full name.
        Validator::extend('full_name', function ($attribute, $value) {

            // This will only accept at least two words
            return preg_match('/^[a-zA-Z.,]+(?: [a-zA-Z,.]+)+$/u', $value);

        });

        // Add the custom validation rule for checking the email domain.
        Validator::extend('email_domain', function ($attribute, $value) {

            // This will only accept the @entergy.com as email domain
            return (explode("@", $value)[1] === "entergy.com");

        });

        // Add the custom validation rule for checking the phone number.
        Validator::extend('phone_number', function($attribute, $value, $parameters) {

            // This will only accept at least two words
            return preg_match('/^[0-9]{10}$/u', $value);

        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
