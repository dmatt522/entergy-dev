<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;


    protected function authenticated(Request $request, $user)
    {
      if ($user->admin == true) {
        return redirect()->action('AdminDashboardController@index');
      }
      return redirect()->route('auctions.index');
      //return redirect('/home');
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     *  Over-ridden the credentials method from the authenticateUsers trait
     *  Remember to take care while upgrading laravel
     */
    public function credentials(Request $request)
    {
        return [
            'email' => $request->email,
            'password' => $request->password,
            'verified' => 1,
        ];
    }

    /**
     *  Over-ridden the reset method from the "SendsPasswordResetEmails" trait
     *  Remember to take care while upgrading laravel
     */
    public function showLoginForm(Request $request)
    {
        // check the session to pre-fill the user email
        if ($request->session()->has('email')) {
            $email = $request->session()->get('email');
            $request->session()->forget('email');
            return view('auth.login')
                ->with('email', $email);
        }

        return view('auth.login');
    }
}
