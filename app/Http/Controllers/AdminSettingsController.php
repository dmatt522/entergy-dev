<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use App\Category;
use App\Businessunit;
use Illuminate\Http\Request;

class AdminSettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'admin']);
    }

    public function index()
    {
        //
        $users = User::all();
        $categories = Category::all();
        $businessunits = Businessunit::all();
        return view('layouts.admin_settings')
                ->with('businessunits', $businessunits)
                ->with('categories', $categories)
                ->with('users', $users);
    }
}
