<?php

namespace App\Http\Controllers;

use App\Auction;
use App\Bid;
use App\CategoryWatch;
use App\Events\NewAuctionCreated;
use App\Events\OutBidSubmitted;
use App\Watch;
use Carbon\Carbon;
use App\Category;
use App\Businessunit;
use App\Image;
use PDF;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuctionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sort_by = 'newest-date';
        if ($request->session()->has('sort_by')) { // if selected the 'sort-by' option
            $sort_by = $request->session()->get('sort_by');
            $request->session()->forget('sort_by');
        }

        $allcount = Auction::open()->count();
        $categories = Category::all();
        $auctions = Auction::open()->get();
        $auctions->map(function ($auc) { // add the highest amount of bid in a given auction
            if (!is_null($auc->highestbid())) {
                $auc['bid_amount'] = $auc->highestbid()->amount;
            } else {
                $auc['bid_amount'] = $auc->min_bid;
            }
            return $auc;
        });

        // rearrange the collection by selected 'sort-by' option
        switch ($sort_by) {
            case 'newest-date' :
                $auctions = $auctions->sortByDesc('created_at');
                break;
            case 'oldest-date' :
                $auctions = $auctions->sortBy('created_at');
                break;
            case 'lowest-price' :
                $auctions = $auctions->sortBy('bid_amount');
                break;
            case 'highest-price' :
                $auctions = $auctions->sortByDesc('bid_amount');
                break;
            default :
                break;
        }

        return view('layouts.user_auctions')
                ->with('auctions', $auctions)
                ->with('categories', $categories)
                ->with('allcount', $allcount)
                ->with('sort_by', $sort_by);
    }

    /**
     * Sort the items on the list by date or price.
     *
     * @return \Illuminate\Http\Response
     */
    public function sort(Request $request)
    {
        return redirect($request->path())
                ->with('sort_by', $request->input('sort-by'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $condition_options = array(
                null => 'Select condition...',
                'Poor' => 'Poor',
                'Good' => 'Good',
                'Like New' => 'Like New',
                'New - Never Been used' => 'New - Never Been used'
        );
        $businessunits = Businessunit::all();
        $businessunits->prepend(['name' => 'Select business unit...', 'id' => null]);
        $categories = Category::all();
        $categories->prepend(['name' => 'Select category...', 'id' => null]);
        return view('layouts.admin_auctions')
                ->with('condition_options', $condition_options)
                ->with('categories', $categories)
                ->with('businessunits', $businessunits);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
                'item_name' => 'required|string|max:255',
                'ir_number' => 'nullable|string|max:8',
                'min_bid' => 'required|numeric',
                'end_date' => 'required|date_format:n/j/Y|after:today',
                'end_time' => 'required|date_format:H:i',
                'item_description' => 'nullable',
                'item_images.*' => 'nullable|image',
                'item_condition' => 'required|string',
                'category_id' => 'required|integer',
                'businessunit_id' => 'required|integer',
                'location' => 'nullable|string',
                'contact_name' => 'required|string',
                'contact_phone' => 'required',
        ]);
        $expires_at = new Carbon($request->end_date . ' ' . $request->end_time, 'America/Chicago');
        $auction = new Auction;
        $auction->user_id = Auth::user()->id;
        $auction->item_name = $request->item_name;
        $auction->ir_number = $request->ir_number;
        $auction->min_bid = $request->min_bid;
        $auction->expires_at = $expires_at->setTimezone('UTC');
        $auction->item_description = $request->item_description;
        $auction->item_condition = $request->item_condition;
        $auction->category_id = $request->category_id;
        $auction->businessunit_id = $request->businessunit_id;
        $auction->location = $request->location;
        $auction->contact_name = $request->contact_name;
        $auction->contact_phone = $request->contact_phone;
        $auction->save();
        if ($request->hasFile('item_images')) {
            $files = $request->file('item_images');
            $featured_set = false;
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $destinationPath = 'public/auction_images';
                    $path = $file->store($destinationPath);
                    $image = new Image;
                    $image->path = $path;
                    $image->auction_id = $auction->id;
                    if ($featured_set == false) {
                        $image->featured = true;
                        $featured_set = true;
                    }
                    $image->save();
                }
            }
        }

        // Dispatch a new event to send the new auction notification to all users.
        event(new NewAuctionCreated($auction));

        return redirect()->action('AdminDashboardController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Auction $auction
     * @return \Illuminate\Http\Response
     */
    public function show(Auction $auction, Request $request)
    {
        $bid_flag = false;
        // check if his/her bid starts
        if ($request->session()->has('bid_flag')) {
            $bid_flag = $request->session()->get('bid_flag');
            $request->session()->forget('bid_flag');
        }

        $bids = $auction->bids()->get();
        $current_bid = Auth::user()->current_bid($auction->id);
        $others_bids = null;

        // remove the current bid of this user
        if (!is_null($bids) && !is_null($current_bid)) {
            $bids = $bids->where('id', '!=', $current_bid->id);
        }

        // pick up only 5 latest bids
        if (!is_null($bids)) {
            $others_bids = $bids->sortByDesc('amount')->take(5);
        }

        // check if the request url contains 'admin'
        $admin_flag = strpos($request->url(), "admin");
        $watch = Watch::where('user_id', Auth::user()->id)
                ->where('auction_id', $auction->id)
                ->first();

        return view('layouts.auction')
                ->with('auction', $auction)
                ->with('bid_flag', $bid_flag)
                ->with('others_bids', $others_bids)
                ->with('admin_flag', $admin_flag)
                ->with('watch', $watch);
    }

    /**
     * Create the new bid.
     *
     * @param  \App\Auction $auction
     * @return \Illuminate\Http\Response
     */
    public function createBid(Request $request, $auction_id)
    {
        $auction = Auction::Open()->find($auction_id);
        if ($auction != null) {
            $this->validate($request, [
                    'bid_price' => 'required|numeric'
            ]);
            $bid = new Bid;
            $bid->amount = $request->input('bid_price');
            $bid->user_id = Auth::user()->id;
            $bid->auction_id = $auction_id;
            $bid->save();

            // Dispatch a new event to send the out-bid notification to another users else.
            event(new OutBidSubmitted($bid));

            return redirect($request->path())
                    ->with('bid_flag', true);
        } else {
            return redirect($request->path())
                    ->with('expired_flag', true);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Auction $auction
     * @return \Illuminate\Http\Response
     */
    public function edit(Auction $auction)
    {
        $condition_options = array(
                null => 'Select condition...',
                'Poor' => 'Poor',
                'Good' => 'Good',
                'Like New' => 'Like New',
                'New - Never Been used' => 'New - Never Been used'
        );
        $businessunits = Businessunit::all();
        $businessunits->prepend(['name' => 'Select business unit...', 'id' => null]);
        $categories = Category::all();
        $categories->prepend(['name' => 'Select category...', 'id' => null]);
        return view('layouts.admin_auctions')
                ->with('auction', $auction)
                ->with('condition_options', $condition_options)
                ->with('categories', $categories)
                ->with('businessunits', $businessunits);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Auction $auction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Auction $auction)
    {
        $this->validate($request, [
                'item_name' => 'required|string|max:255',
                'ir_number' => 'nullable|string|max:8',
                'min_bid' => 'required|numeric',
                'end_date' => 'required|date_format:n/j/Y',
                'end_time' => 'required|date_format:H:i',
                'item_description' => 'nullable',
                'item_images.*' => 'nullable|image',
                'item_condition' => 'required|string',
                'category_id' => 'required|integer',
                'businessunit_id' => 'required|integer',
                'location' => 'nullable|string',
                'contact_name' => 'required|string',
                'contact_phone' => 'required',
                'buyer_name' => 'nullable|string|max:255',
        ]);
        $expires_at = new Carbon($request->end_date . ' ' . $request->end_time, 'America/Chicago');
        $auction->item_name = $request->item_name;
        $auction->ir_number = $request->ir_number;
        $auction->min_bid = $request->min_bid;
        $auction->buyer_name = $request->buyer_name;
        if ($auction->ended_at != null && $auction->expires_at < $expires_at) {
            $auction->ended_at = null;
            $auction->buyer_name = null;
            if ($auction->winnerBid() != null) {
                $clearwinner = $auction->winnerBid();
                $clearwinner->winner = false;
                $clearwinner->save();
            }
        }
        $auction->expires_at = $expires_at->setTimezone('UTC');
        $auction->item_description = $request->item_description;
        $auction->item_condition = $request->item_condition;
        $auction->category_id = $request->category_id;
        $auction->businessunit_id = $request->businessunit_id;
        $auction->location = $request->location;
        $auction->contact_name = $request->contact_name;
        $auction->contact_phone = $request->contact_phone;
        $auction->save();
        if ($request->hasFile('item_images')) {
            $files = $request->file('item_images');
            if ($auction->images()->count() == 0) {
                $featured_set = false;
            } else {
                $featured_set = true;
            }
            foreach ($files as $file) {
                if ($file->isValid()) {
                    $destinationPath = 'public/auction_images';
                    $path = $file->store($destinationPath);
                    $image = new Image;
                    $image->path = $path;
                    $image->auction_id = $auction->id;
                    if ($featured_set == false) {
                        $image->featured = true;
                        $featured_set = true;
                    }
                    $image->save();
                }
            }
        }
        return redirect()->action(
                'AuctionController@show', ['id' => $auction->id]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Auction $auction
     * @return \Illuminate\Http\Response
     */
    public function destroy(Auction $auction)
    {
        //
    }

    public function close(Request $request)
    {
        $close = Artisan::call('auctions:close', [
                'id' => $request->id, 'bid' => $request->bid_id
        ]);
        return redirect()->action(
                'AuctionController@show', ['id' => $request->id]
        );
    }

    public function bos($auctionId)
    {
        $auction = Auction::find($auctionId);
        if ($auction->ended_at != null && $auction->winnerBid() != null && (Auth::user() == $auction->winnerBid()->author() || Auth::user()->admin == 1)) {
            $data = [
                    'buyer_name' => $auction->buyer_name,
                    'ended_at' => $auction->ended_at,
                    'author_name' => $auction->winnerBid()->author()->name,
                    'amount' => $auction->winnerBid()->amount,
                    'item_name' => $auction->item_name
            ];

            $pdf = PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif', 'isHtml5ParserEnabled' => true])->loadView('pdf.bill_of_sale', $data);
            return $pdf->download('bos.pdf');

            /*return view('pdf.bill_of_sale')
                ->with('auction', $auction);*/
        } else {
            return redirect()->action(
                    'AuctionController@show', ['id' => $auctionId]);
        }
    }

    public function getCategory(Request $request, $categoryId)
    {
        $sort_by = 'newest-date';
        if ($request->session()->has('sort_by')) { // if selected the 'sort-by' option
            $sort_by = $request->session()->get('sort_by');
            $request->session()->forget('sort_by');
        }

        $allcount = Auction::open()->count();
        $categories = Category::all();
        $category_watch = CategoryWatch::where('user_id', Auth::user()->id)
                ->where('category_id', $categoryId)
                ->first();
        $auctions = Category::find($categoryId)->auctions()->where('ended_at', null);
        $auctions->map(function ($auc) { // add the highest amount of bid in a given auction
            if (!is_null($auc->highestbid())) {
                $auc['bid_amount'] = $auc->highestbid()->amount;
            } else {
                $auc['bid_amount'] = $auc->min_bid;
            }
            return $auc;
        });

        // rearrange the collection by selected 'sort-by' option
        switch ($sort_by) {
            case 'newest-date' :
                $auctions = $auctions->sortByDesc('created_at');
                break;
            case 'oldest-date' :
                $auctions = $auctions->sortBy('created_at');
                break;
            case 'lowest-price' :
                $auctions = $auctions->sortBy('bid_amount');
                break;
            case 'highest-price' :
                $auctions = $auctions->sortByDesc('bid_amount');
                break;
            default :
                break;
        }

        return view('layouts.user_auctions')
                ->with('auctions', $auctions)
                ->with('categories', $categories)
                ->with('category_watch', $category_watch)
                ->with('allcount', $allcount)
                ->with('activeId', $categoryId)
                ->with('sort_by', $sort_by);
    }
}
