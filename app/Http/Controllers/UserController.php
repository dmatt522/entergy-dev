<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display the user info.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('layouts.user_info')
            ->with('user', $user);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|full_name',
            'phone' => 'nullable|phone_number',
            'password' => 'nullable|min:8',
        ]);
    }

    /**
     * Update the specified user info.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = Auth::user();
        $user->name = $request->input('name');
        $user->phone = "+1" . $request->input('phone');
        if (!is_null($request->input('password'))) {
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return redirect($request->path())
            ->withInput();
    }

    // Autocomplete for Form
    public function autocomplete(Request $request)
    {
        $term = $request->get('term');

        $results = array();

        $queries = User::admin('false')
            ->where('name', 'LIKE', '%' . $term . '%')
            ->take(5)->get();

        foreach ($queries as $query) {
            $results[] = ['id' => $query->id, 'value' => $query->name];
        }
        return response()->json($results);
    }

    public function revokeAdmin($id)
    {
        $user = User::find($id);
        if ($user == null) {
          return redirect()->action('AdminSettingsController@index');
        }
        $user->admin = false;
        $user->save();
        return redirect()->action('AdminSettingsController@index');
    }

    public function addAdmin(Request $request)
    {
        $user = User::find($request->id);
        if ($user == null) {
          return redirect()->action('AdminSettingsController@index');
        }
        $user->admin = true;
        $user->save();
        return redirect()->action('AdminSettingsController@index');
    }
}
