<?php

namespace App\Http\Controllers;

use File, Response;
use Illuminate\Http\Request;
use App\Image;

class ImageController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the image from storage folder.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($filename)
    {
        $path = storage_path() . '/app/public/auction_images/' . $filename;

        if (!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);
        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);
        return $response;
    }

    public function setFeatured(Request $request)
    {
        $newFeatured = Image::find($request->id);
        $auction = $newFeatured->auction_id;
        $oldFeatured = Image::where([['auction_id', $auction], ['featured', 1]])->first();
        if ($oldFeatured != $newFeatured) {
            $oldFeatured->featured = 0;
            $oldFeatured->save();
            $newFeatured->featured = 1;
            $newFeatured->save();
        }
        $response = Response::make('Ok', 200);
    }

    public function delete(Request $request)
    {
        $toDelete = Image::find($request->id);
        $path = storage_path() . '/app/' . $toDelete->path;
        if (File::exists($path)) {
            File::delete($path);
        }
        $toDelete->delete();
        $response = Response::make('Ok', 200);
    }
}
