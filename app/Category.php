<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    // Return the auctions on this Category
    public function auctions()
    {
        return $this->hasMany('App\Auction')->get();
    }
}
