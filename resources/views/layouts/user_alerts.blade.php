<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <title>Entergy</title>
  </head>
  <body>
    {{-- This is the Navbar, ideally it must change based on the User role--}}
    @include('shared.navbar')
    {{-- End of the Navbar. Main container starts --}}

    <div class="container">
      <h1>Alerts</h1>
      <hr>
      <h3>Categories</h3>
      <div class="row">
        <ul class="navbar-nav">
          @foreach($subscribed_categories as $category)
            <li style="margin-right: 30px;">
              <a href="{{ route('auction.category', $category->id) }}">
                {{ $category->name }} <span class="badge">{{ $category->auctions()->where('ended_at', null)->count() }}</span>
              </a>
            </li>
          @endforeach
        </ul>
      </div>
      <div class="row">
        @each('user.category-new-item', $category_new_auctions, 'auction', 'user.auction-empty-list')
      </div>
      <hr>
      <h3>My Bids</h3>
      <div class="row">
        @each('user.alert-list-item', $alert_auctions, 'auction', 'user.auction-empty-list')
      </div>

      {{-- This is the footer section --}}
      @include('shared.footer')
      {{-- End of footer --}}
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
  </body>
</html>
