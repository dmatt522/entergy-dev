<div class="row">
  <div class="col-xs-7">Someone else bid ${{ $other_bid->amount }}</div>
  <div class="col-xs-5">{{ $other_bid->created_at->timezone('America/Chicago')->format('n/j/y \a\t g:i:s a T') }}</div>
</div>
