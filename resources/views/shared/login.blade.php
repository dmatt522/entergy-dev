<div class="panel panel-default">
  <div class="panel-heading"><strong>Log in to Entergy Bid Board</strong></div>
  <div class="panel-body">
    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control input-lg" name="email" value="{{ old('email') }}" required autofocus
               placeholder="Entergy email address...">

        @if ($errors->has('email'))
          <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control input-lg" name="password" required placeholder="Your password...">

        @if ($errors->has('password'))
          <span class="help-block">
            <strong>{{ $errors->first('password') }}</strong>
          </span>
        @endif
      </div>

      <div class="form-group">
        <p>
          <button type="submit" class="btn btn-primary btn-block btn-lg">
            Log In
          </button>
        </p>
      </div>
    </form>
  </div>
</div>
