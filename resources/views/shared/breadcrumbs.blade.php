<!-- BREADCRUMBS -->
<div class="container">
  <ol class="breadcrumb">
    <li><a href="{{ route('auctions.index') }}">Bid Board</a></li>
    <li><a href="{{ route('auction.category', $auction->category()->id ) }}">{{$auction->category()->name}}</a></li>
    <li class="active"><a href="{{ action('AuctionController@show', ['id' => $auction->id]) }}">{{$auction->item_name}}</a></li>
  </ol>
</div>
