<div class="modal fade modal-emails" id="emailWinnerModal" tabindex="-1" role="dialog" aria-labelledby="Email Winner">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Email Auction Winner</h4>
      </div>
      <div class="modal-body dark-bg">
        <div class="list-group">
          <a href="{{ route('admin.editEmail', ['id' => $auction->id, 'type' => 'winner']) }}" class="list-group-item">
            <h4 class="list-group-item-heading">Congrats on your win!</h4>
            <p class="list-group-item-text">Includes payment and pick up info</p>
          </a>
          <a href="{{ route('admin.editEmail', ['id' => $auction->id, 'type' => 'pickup']) }}" class="list-group-item">
            <h4 class="list-group-item-heading">Please pick up your item</h4>
            <p class="list-group-item-text">Remind winner to pick up item</p>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>
