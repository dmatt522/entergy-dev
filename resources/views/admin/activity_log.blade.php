<div class="row admin-auction-data">


  <div class="col-sm-12">

    <div class="jumbotron">

      <h3>Auction Activity</h3>
      <h5 class="small text-muted"><span class="text-danger">*</span>These fields are only visible to administrators</h5>

      <div cass="clearfix spacer"></div>

      <table class="table table-striped bids">

        @each('admin.activity_log_bid',$auction->bids()->orderBy('created_at', 'desc')->orderBy('amount', 'desc')->get(), 'bid')
        <tr class="admin">

          <td colspan="3">
            <strong>{{$auction->createdBy()->name}}</strong> published auction
          </td>
          <td>
            {{$auction->created_at->timezone('America/Chicago')->toFormattedDateString()}} {{$auction->created_at->timezone('America/Chicago')->format('g:i a T')}}
          </td>
        </tr>
      </table>

    </div>

  </div>
</div>
